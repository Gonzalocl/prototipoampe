package com.alan.prototipo;

import android.app.Activity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public class MotorThread extends Thread implements RobotFeature {

    private static final char END = '0';
    private static final char SIDE_R = '1';
    private static final char SIDE_L = '2';

    private Queue<Float> queue;
    private Queue<Character> side;
    private Semaphore queueCount;
    private Semaphore queueMutex;

    private String ip;
    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;

    private float speedR;
    private float speedL;

    public MotorThread (String ip) throws IOException {
        socket = new Socket(ip, RobotFeatures.motor.port);
        try {
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            socket.close();
            throw new IOException();
        }

        queue = new LinkedList<>();
        side = new LinkedList<>();
        queueCount = new Semaphore(0);
        queueMutex = new Semaphore(1);
        speedR = 0;
        speedL = 0;


    }

    @Override
    public void run() {
        while (true) {
            try {
                queueCount.acquire();
                // TODO si el acquire lanza una excepcion? hay que release?
                queueMutex.acquire();
                float send = queue.poll();
                int sie = side.poll();
                queueMutex.release();

                try {

                    byte[] a = ByteBuffer.allocate(4).putFloat(send).array();

                    out.writeByte(sie);
                    out.writeByte(a[0]);
                    out.writeByte(a[1]);
                    out.writeByte(a[2]);
                    out.writeByte(a[3]);

                    //Thread.sleep(1*1000);


                    //out.writeFloat(send);
                    //out.writeInt(sie);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    public boolean sendSpeedR(float x) {
        // TODO Throw illegal state exception?
        // TODO operaciones bloqueantes en el hilo principal?
        speedR = x;
        if (out == null) return false;
        try {
            queueMutex.acquire();
            queue.add(x);
            side.add(SIDE_R);
            queueCount.release();
            queueMutex.release();
            return true;
        } catch (InterruptedException e) {
            return false;
        }

    }

    public boolean sendSpeedL(float x) {
        // TODO Throw illegal state exception?
        speedL = x;
        if (out == null) return false;
        try {
            queueMutex.acquire();
            queue.add(x);
            side.add(SIDE_L);
            queueCount.release();
            queueMutex.release();
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }


    public float getSpeedR() {
        return speedR;
    }

    public float getSpeedL() {
        return speedL;
    }

    @Override
    public void startFeature(Activity ctx) {
        start();
    }

    @Override
    public void stopFeature() {

    }
}
