package com.alan.prototipo;

import java.io.IOException;

public class RobotFeatureFactory {
    public static RobotFeature newRobotFeature(RobotFeatures f, String ip) {
        switch (f) {
            case motor:
                try {
                    return new MotorThread(ip);
                } catch (IOException e) {
                    return null;
                }
            case camera:
                try {
                    return new CameraThread(ip);
                } catch (IOException e) {
                    return null;
                }
            case ultrasonic:
                try {
                    return new UltrasonicThread(ip);
                } catch (IOException e) {
                    return null;
                }
        }
        return null;
    }
}
