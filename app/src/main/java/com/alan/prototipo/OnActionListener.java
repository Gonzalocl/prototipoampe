package com.alan.prototipo;

import android.view.View;

public interface OnActionListener {
    void onAction(View v, float x);
}
