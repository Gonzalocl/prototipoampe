package com.alan.prototipo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Main extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback {

    public static final int STORAGE_CODE = 1000;
    private static boolean storagePermission = false;

    private static Context AppContext = null;

    private static final String KEY_PREF_IP = "ip";

    private boolean validIp(String ip) {
        return ip != null && Patterns.IP_ADDRESS.matcher(ip).matches();
    }



    //TODO
    private GLSurfaceView ogl;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        //tODO
//        ogl = new MyGLSurfaceView(this);
//        setContentView(ogl);


        AppContext = getApplicationContext();
        setContentView(R.layout.mainlayout);

        reqPermissions();

        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        String ip = preferences.getString(KEY_PREF_IP, null);
        if (ip != null) {
            EditText et = findViewById(R.id.input_ip);
            et.setText(ip);
        }

        final App.ResultCallBack callBack = new App.ResultCallBack() {
            @Override
            public void result() {
                final App app = App.getOnlyInstance();
                if (app.isAppUp()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(getApplicationContext(), Control.class);
                            for (RobotFeatures f : RobotFeatures.values()) {
                                if (!app.isFeatureUp(f)) {
                                    Toast t = Toast.makeText(getApplicationContext(), R.string.feature_down, Toast.LENGTH_SHORT);
                                    t.show();

                                }
                            }
                            startActivity(i);
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast t = Toast.makeText(getApplicationContext(), R.string.connection_failed, Toast.LENGTH_SHORT);
                            t.show();
                        }
                    });
                }

            }
        };

        Button connect = findViewById(R.id.b_connect_str);
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = findViewById(R.id.input_ip);
                String ip  = et.getText().toString();

                if(!validIp(ip)) {
                    Toast t = Toast.makeText(getApplicationContext(), R.string.ip_invalid, Toast.LENGTH_SHORT);
                    t.show();
                }
                else {
                    SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(KEY_PREF_IP, ip);
                    editor.apply();

                    App app = App.getOnlyInstance();
                    app.connect(ip, callBack);
                }

            }
        });


    }


    private void reqPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_CODE);
            storagePermission = false;
        } else {
            storagePermission = true;
        }
    }

    public static Context getAppContext() {
        return AppContext;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    storagePermission = true;
                }
                break;
        }
    }

    public static boolean isStoragePermissionGranted() {
        return storagePermission;
    }
}
