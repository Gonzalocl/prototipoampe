package com.alan.prototipo;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


//TODO comprobar errores
public class PhotoSaver {


    private File folder;



    public PhotoSaver (String folderPath) throws IOException {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            throw new IOException("STORAGE");
        }


        //TODO check errors

        folder = new File("/storage/0CAE-D7BC", folderPath);
        if (!folder.exists()) {
            folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), folderPath);
        }
        folder.mkdirs();

    }


    public void save(String fileName, byte[] data) throws IOException {
        File file = new File(folder, fileName);
        file.createNewFile();
        BufferedOutputStream buf = new BufferedOutputStream(new FileOutputStream(file));
        buf.write(data);
        buf.flush();
        buf.close();
    }

    public void close() {

    }





}
