package com.alan.prototipo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class ImgClient implements SensorEventListener {


    private static final double factor = 0.05;
    private double threshold;
    private static final int IMG_PORT = 2525;
    private static final byte TAKE_PIC = 1;
    private static final byte END = 0;

    private String ip;
    private ImageView iv;



    private Socket socket;
    private DataOutputStream imgSocketOut;
    private DataInputStream imgSocketIn;

    private boolean takingPics;
    private Semaphore mutex;

    private SensorManager sm;


    public ImgClient(String ip, ImageView imageView) {
        this.ip = ip;
        this.iv = imageView;
        try {
            this.socket = new Socket(ip, IMG_PORT);
            this.imgSocketOut = new DataOutputStream(socket.getOutputStream());
            this.imgSocketIn = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }



        this.takingPics = false;
        this.mutex = new Semaphore(1);


        this.sm = (SensorManager) Main.getAppContext().getSystemService(Context.SENSOR_SERVICE);
        Sensor accel = sm.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        this.threshold = accel.getMaximumRange()*factor;
        this.sm.registerListener(this, accel, SensorManager.SENSOR_DELAY_NORMAL);
    }




    private void takePic() {
        Log.e("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB", "2222222222222222222222222222222222222222222");

//        setImg(receiveImg());

        try {
            Thread.sleep(1*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            mutex.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        takingPics = false;
        mutex.release();
    }


    private byte[] receiveImg() {
        byte[] img = null;
        try {
            imgSocketOut.writeByte(TAKE_PIC);
            int size = (int) imgSocketIn.readLong();
            img = new byte[size];
            imgSocketIn.readFully(img);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

    private void setImg(byte[] img) {
        iv.setImageBitmap(BitmapFactory.decodeByteArray(img, 0, img.length));
    }



    public void close() {
        sm.unregisterListener(this);
        try {
            imgSocketOut.writeByte(END);
            imgSocketIn.close();
            imgSocketOut.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.values[0] > threshold || event.values[1] > threshold || event.values[2] > threshold) {
            Log.e("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11111111111111111111111111111111111111111111");
            try {
                mutex.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!takingPics) {
                takingPics = true;
                mutex.release();
                takePic();
            } else {
                mutex.release();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
