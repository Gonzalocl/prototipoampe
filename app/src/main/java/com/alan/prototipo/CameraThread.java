package com.alan.prototipo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Semaphore;

public class CameraThread implements RobotFeature {

    //this can be set to true to enable saving frames to storage
    private static final boolean SAVE_VIDEO = true;


    private static final boolean SHOW_HW_ACCL = true;
    private boolean checked = false;


    private static final boolean SAVE_PHOTOS = true;
    private static final boolean SHOW_FPS = false;


    private static final float factor = 0.05f;

    private static final char TAKE_PIC = '1';
    private static final char END = '0';

    private Activity ctx;
    private ImageView iv;
    private volatile boolean takingPics;
    private Semaphore mutex;

    private String ip;
    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;

    private PhotoSaver ps;
    private PhotoSaver photoSaver;

    private BackTap backTap;

    private long firstFrameTime;
    private long lastFrameTime;
    private long currentFrameTime;
    private double FPS;
    private double averageFPS;
    private int showInterval = 20;

    public CameraThread (String ip) throws IOException {
        this.takingPics = false;
        this.ip = ip;
        socket = new Socket(ip, RobotFeatures.camera.port);
        try {
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            socket.close();
            throw new IOException();
        }
        try {
            in = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            socket.close();
            throw new IOException();
        }
        this.mutex = new Semaphore(1);



        this.photoSaver = null;
        this.ps = null;
        this.backTap = null;
        if (Main.isStoragePermissionGranted()) {
            if (SAVE_VIDEO) {
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                String format = sdf.format(today);
                try {
                    this.ps = new PhotoSaver(String.format("Alan/video/%s/", format));
                } catch (IOException e) {
                    // TODO toast no hay almacenamiento
                    this.ps = null;
                }
            }

            if (SAVE_PHOTOS) {
                try {
                    this.photoSaver = new PhotoSaver("Alan/photos");
                    this.backTap = new BackTap(factor, new BackTap.OnBackTapListener() {
                        @Override
                        public void onBackTap() {
                            takePic();
                        }
                    });
                } catch (IOException e) {
                    this.photoSaver = null;
                }
            }


        }

    }

    public void startVideo() {
        if (iv == null) return;
        try {
            mutex.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!takingPics) {
            takingPics = true;
            mutex.release();
            Thread t = new Thread() {
                @Override
                public void run() {
                    processVideo();
                }
            };
            t.start();
        } else  {
            mutex.release();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (!checked && iv.isAttachedToWindow()) {
                    checked = true;
                    if (SHOW_HW_ACCL && iv.isHardwareAccelerated()) {
                        ctx.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast t = Toast.makeText(ctx, "Visor es aclerado por hardware", Toast.LENGTH_SHORT);
                                t.show();
                            }
                        });
                    }
                }




            }
        }).start();

    }

    public void takePic() {
        takingPics = true;
    }

    private byte [] receivePic() {
        byte[] pic = null;
        try {
            out.writeByte(TAKE_PIC);
//            int size = (int) in.readLong();
//
//
            byte[] l = new byte[4];
//            l[7] = in.readByte();
//            l[6] = in.readByte();
//            l[5] = in.readByte();
//            l[4] = in.readByte();
            l[3] = in.readByte();
            l[2] = in.readByte();
            l[1] = in.readByte();
            l[0] = in.readByte();
//            l[0] = in.readByte();
//            l[1] = in.readByte();
//            l[2] = in.readByte();
//            l[3] = in.readByte();
            ByteBuffer bbw = ByteBuffer.wrap(l);
            int size = bbw.getInt();


            pic = new byte[size];
            in.readFully(pic);


        } catch (IOException e) {
            e.printStackTrace();
        }
        return pic;
    }

    private void setPic(final byte[] pic) {
        final Bitmap bitmap = BitmapFactory.decodeByteArray(pic, 0, pic.length);
        ctx.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                iv.setImageBitmap(bitmap);
            }
        });
    }

    private void process() {

        byte[] pic = receivePic();
        if (pic == null) {
            try {
                mutex.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            takingPics = false;
            mutex.release();
            ctx.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast t = Toast.makeText(ctx, R.string.no_pic, Toast.LENGTH_SHORT);
                    t.show();
                }
            });
        } else {
            setPic(pic);
            try {
                mutex.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            takingPics = false;
            mutex.release();
        }
    }
    private void processVideo() {

        int i = 0;
        lastFrameTime = System.currentTimeMillis();
        firstFrameTime = lastFrameTime;
        while (true) {
            byte[] pic = receivePic();
            if (pic == null) {
                ctx.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast t = Toast.makeText(ctx, R.string.no_pic, Toast.LENGTH_SHORT);
                        t.show();
                    }
                });
            } else {
                setPic(pic);

                if (takingPics && photoSaver != null) {
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                    String format = sdf.format(today);
                    try {
                        photoSaver.save(String.format("%s.jpg", format), pic);
                    } catch (IOException e) {
                        //TODO toast
                        e.printStackTrace();
                    }
                    takingPics = false;
                }




                try {
                    if (ps != null) {
                        ps.save(String.format("frame%06d.jpg", i), pic);
                        i++;
                    }
                } catch (IOException e) {
//                    TODO toast
                    e.printStackTrace();
                }
            }
            if (SHOW_FPS) {
                currentFrameTime = System.currentTimeMillis();
                if ((i % showInterval) == 0) {
                    FPS = 1000.0f / (currentFrameTime - lastFrameTime);
                    averageFPS = 1000.0f * i / (currentFrameTime - firstFrameTime);
                    ctx.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast t = Toast.makeText(ctx, String.format("FPS: %.2f, Meida: %.2f", FPS, averageFPS), Toast.LENGTH_SHORT);
                            t.show();
                        }
                    });
                }
                lastFrameTime = currentFrameTime;
            }





        }
    }

    @Override
    public void startFeature(final Activity ctx) {
        iv = ctx.findViewById(R.id.camera);
        this.ctx = ctx;
    }

    @Override
    public void stopFeature() {
        //stop thread and close things
        //unregister listeners
    }
}
