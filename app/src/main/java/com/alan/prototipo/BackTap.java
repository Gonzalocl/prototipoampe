package com.alan.prototipo;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class BackTap implements SensorEventListener {

    private float factor;
    private float threshold;

    private SensorManager sensorManager;

    private OnBackTapListener onBackTapListener;

    public BackTap(float factor, OnBackTapListener onBackTapListener) {
        this.factor = factor;
        this.onBackTapListener = onBackTapListener;
        this.sensorManager = (SensorManager) Main.getAppContext().getSystemService(Context.SENSOR_SERVICE);
        Sensor accel = this.sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        this.threshold = 0;
        if (accel != null) {
            this.sensorManager.registerListener(this, accel, SensorManager.SENSOR_DELAY_NORMAL);
            this.threshold = accel.getMaximumRange()*this.factor;
        }

    }



    public void stop() {
        this.sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.values[0] > threshold || event.values[1] > threshold || event.values[2] > threshold) {
            onBackTapListener.onBackTap();
        }

    }















    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public interface OnBackTapListener {
        void onBackTap();
    }

}
