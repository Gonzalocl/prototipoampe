package com.alan.prototipo;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class UltrasonicThread extends Thread implements RobotFeature {

    private static final char DANGER = '1';
    private static final char NO_DANGER = '0';

    private String ip;
    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;

    private boolean inDanger;

    private boolean end;

    private Vibrator vibrator;
    private VibrationEffect effect;

    public UltrasonicThread(String ip) throws IOException {
        this.ip = ip;
        socket = new Socket(ip, RobotFeatures.ultrasonic.port);
        try {
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            socket.close();
            throw  new IOException();
        }
        try {
            in = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            socket.close();
            throw new IOException();
        }


        end = false;
        inDanger = false;
        vibrator = (Vibrator) Main.getAppContext().getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            effect = VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE);
        }
    }

    @Override
    public void run() {
        byte c = 'z';
        while (!end) {
            try {
                c = in.readByte();
            } catch (IOException e) {
                end = true;
            }

            switch (c) {
                case DANGER:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vibrator.vibrate(effect);
                    } else {
                        vibrator.vibrate(500);
                    }
                    setInDanger();
                    break;
                case NO_DANGER:
                    vibrator.cancel();
                    setNoDanger();
                    break;
            }



        }
    }


    public boolean isInDanger() {
        return inDanger;
    }

    public void setInDanger() {
        this.inDanger = true;
    }
    public void setNoDanger() {
        this.inDanger = false;
    }

    @Override
    public void startFeature(Activity ctx) {
        start();
    }

    @Override
    public void stopFeature() {
        end = true;
    }
}
