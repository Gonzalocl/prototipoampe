package com.alan.prototipo;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;

public class Speedometer extends GLSurfaceView {


    private static final boolean SHOW_HW_ACCL = true;

    private final SpeedometerRenderer renderer;

    private float speed = 0;
    private static final float MIN_ANGLE = 180;
    private static final float MAX_ANGLE = -90;
    private static final float RANGE = MAX_ANGLE-MIN_ANGLE;

    public Speedometer(Context context) {
        super(context);
        setEGLContextClientVersion(2);
        super.setZOrderOnTop(true);
        super.setEGLConfigChooser(8 , 8, 8, 8, 16, 0);
        super.getHolder().setFormat(PixelFormat.RGBA_8888);
        renderer = new SpeedometerRenderer();
        setRenderer(renderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        renderer.setAngle(MIN_ANGLE);
    }

    public Speedometer(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEGLContextClientVersion(2);
        super.setZOrderOnTop(true);
        super.setEGLConfigChooser(8 , 8, 8, 8, 16, 0);
        super.getHolder().setFormat(PixelFormat.RGBA_8888);
        setBackgroundResource(R.drawable.ic_speedbacground);
        renderer = new SpeedometerRenderer();
        setRenderer(renderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        renderer.setAngle(MIN_ANGLE);
    }


    public void setSpeed(float s) {
        if (s > 0 ) s = -s;
        this.speed = s;
        float angle = s*RANGE + MIN_ANGLE;
        renderer.setAngle(angle);
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (SHOW_HW_ACCL && isHardwareAccelerated()) {
            final Activity control = App.getOnlyInstance().getControl();
            if (control != null) {
                control.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast t = Toast.makeText(control, "Velocimetro acelerado por hardware", Toast.LENGTH_SHORT);
                        t.show();
                    }
                });
            } else {
                Log.d("HW", "Speedometer is hardware accelerated");
            }
        }
    }
}
