package com.alan.prototipo;

public enum RobotFeatures {

    motor(0, 5002, true),
    camera(1, 5001, true),
    ultrasonic(2, 5003, true);

    public final int index;
    public final int port;
    public final boolean required;


    RobotFeatures (int index, int port, boolean required) {
        this.index = index;
        this.port = port;
        this.required = required;
    }
}
