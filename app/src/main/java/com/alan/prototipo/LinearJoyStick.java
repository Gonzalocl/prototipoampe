package com.alan.prototipo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.animation.DynamicAnimation;
import android.support.animation.FloatValueHolder;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class LinearJoyStick extends View {

    private static final int SIZE = 20;

    int id = 0;
    boolean hasTouch = false;

    Paint paint;
    Paint line;

    FloatValueHolder x;
    SpringAnimation spring;
    SpringForce force;

    private OnActionListener listener;

    public LinearJoyStick(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inicialize();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        int action = event.getActionMasked();

        switch (action) {
            // TODO indexx???? mirar en controls
            case MotionEvent.ACTION_DOWN: {
                spring.cancel();
                id = event.getPointerId(0);
                float pos = event.getY(0);
                if (pos > (getHeight()-SIZE))
                    pos = getHeight()-SIZE;
                else if (pos < SIZE)
                    pos = 0;
                x.setValue(pos);
                if (listener != null) listener.onAction(this, (pos-(getHeight()-SIZE)/2.0f)/((getHeight()-SIZE)/2.0f)*-1);
                hasTouch = true;
                break;
            }
            case MotionEvent.ACTION_UP: {
                hasTouch = false;
                spring.start();
                if (listener != null) listener.onAction(this, 0);
                if (listener != null) listener.onAction(this, 0);
                if (listener != null) listener.onAction(this, 0);
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                int index = getPointerById(event);
                float pos = event.getY(index);
                if (pos > (getHeight()-SIZE))
                    pos = getHeight()-SIZE;
                else if (pos < SIZE)
                    //TODO bug en los graficos
                    pos = 0;
                x.setValue(pos);
                if (listener != null) listener.onAction(this, (pos-(getHeight()-SIZE)/2.0f)/((getHeight()-SIZE)/2.0f)*-1);
                break;
            }
        }

        postInvalidate();
        return true;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(getWidth()/2, 0, getWidth()/2, getHeight(), line);
        canvas.drawRect(0, x.getValue()+SIZE, getWidth(), x.getValue()-SIZE, paint);
    }

    private int getPointerById(@Nullable MotionEvent event) {
        for (int i = 0; i < event.getPointerCount(); i++) {
            if (id == event.getPointerId(i))
                return i;
        }

        id = event.getPointerId(0);
        return 0;
    }

    private void inicialize() {

        //33AA91
        paint = new Paint();
        paint.setARGB(175, 0x33, 0xAA, 0x91);
        line = new Paint();
        line.setARGB(255, 200, 200, 200);
        line.setStrokeWidth(10);
        line.setStyle(Paint.Style.STROKE);

        x = new FloatValueHolder();
        force = new SpringForce()
                .setDampingRatio(SpringForce.DAMPING_RATIO_HIGH_BOUNCY)
                .setStiffness(SpringForce.STIFFNESS_LOW);
        spring = new SpringAnimation(x).setSpring(force);
        spring.addUpdateListener(new DynamicAnimation.OnAnimationUpdateListener() {
            @Override
            public void onAnimationUpdate(DynamicAnimation dynamicAnimation, float v, float v1) {
                postInvalidate();
            }
        });

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        x.setValue(getHeight()/2);
        force.setFinalPosition(getHeight()/2);
    }

    public void setOnActionListener(OnActionListener listener) {
        this.listener = listener;
    }


}
