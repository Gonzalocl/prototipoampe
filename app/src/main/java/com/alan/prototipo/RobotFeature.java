package com.alan.prototipo;

import android.app.Activity;

public interface RobotFeature {
    void startFeature(Activity ctx);
    void stopFeature();
}
