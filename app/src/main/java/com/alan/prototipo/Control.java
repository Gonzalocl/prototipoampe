package com.alan.prototipo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

public class Control extends Activity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control);

        App app = App.getOnlyInstance();
        app.setControl(this);

        final CameraThread cam = (CameraThread) app.getRobotFeatures(RobotFeatures.camera);
        cam.startFeature(this);
        cam.startVideo();

        final MotorThread motor = (MotorThread) app.getRobotFeatures(RobotFeatures.motor);
        motor.startFeature(this);

        UltrasonicThread ultrasonic = (UltrasonicThread) app.getRobotFeatures(RobotFeatures.ultrasonic);
        ultrasonic.startFeature(this);

        final Speedometer speedometer = findViewById(R.id.speedometer);


        LinearJoyStick r = findViewById(R.id.rStick);
        r.setOnActionListener(new OnActionListener() {
            @Override
            public void onAction(View v, float x) {
                motor.sendSpeedR(x);
                float speed = (x + motor.getSpeedL())/2;
                speedometer.setSpeed(speed);
                speedometer.requestRender();
            }
        });

        LinearJoyStick l = findViewById(R.id.lStick);
        l.setOnActionListener(new OnActionListener() {
            @Override
            public void onAction(View v, float x) {
                motor.sendSpeedL(x);
                float speed = (x + motor.getSpeedR())/2;
                speedometer.setSpeed(speed);
                speedometer.requestRender();
            }
        });

    }
}
