package com.alan.prototipo;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class App extends Thread {

    private static final int PORT_CONTROL = 5000;

    private Socket controlSocket;

    private DataOutputStream controlOut;

    private RobotFeature[] features;

    private boolean appState;
    private boolean controlState;
    private boolean[] featureState;

    private static App onlyInstance = new App();

    private String robotIP;

    private Control control = null;

    // singleton
    private App() {
        this.controlSocket = null;

        this.controlOut = null;

        features = new RobotFeature[RobotFeatures.values().length];

        this.appState = false;
        this.controlState = false;
        featureState = new boolean[RobotFeatures.values().length];


    }

    public static App getOnlyInstance() {
        return onlyInstance;
    }


    @Override
    public void run() {
        //TODO
    }


    // connect to robot
    public void connect(final String ip, final App.ResultCallBack callBack) {
        Thread t = new Thread() {
            @Override
            public void run() {
                robotIP = ip;
                try {
                    controlSocket = new Socket(robotIP, PORT_CONTROL);
                    controlOut = new DataOutputStream(controlSocket.getOutputStream());
                    controlState = true;
                } catch (IOException e) {
//                    throw new RuntimeException("=========================MAIN==========================");
                    appState = false;
                    controlState = false;
                    callBack.result();
                    return;
                }

                for (RobotFeatures f : RobotFeatures.values()) {
                    if (f.required) {
                        features[f.index] = RobotFeatureFactory.newRobotFeature(f, robotIP);
                        if (features[f.index] != null) {
                            featureState[f.index] = true;
                        } else {
//                            throw new RuntimeException("=========================" + f + "==========================");
                            appState = false;
                            featureState[f.index] = false;
                            disconnect();
                            callBack.result();
                            return;
                        }
                    }
                }

                // minimum available
                appState = true;

                for (RobotFeatures f : RobotFeatures.values()) {
                    if (!f.required) {
                        features[f.index] = RobotFeatureFactory.newRobotFeature(f, robotIP);
                        if (features[f.index] != null) {
                            featureState[f.index] = true;
                        } else {
                            //throw new RuntimeException();
                            featureState[f.index] = false;
                        }
                    }
                }

                getOnlyInstance().start();
                callBack.result();

            }
        };
        t.start();
    }

    // free resources and stop threads
    public void disconnect() {
        try {
            if (controlOut != null) controlOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (controlSocket != null) controlSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        appState = false;
        controlState = false;
        for (RobotFeatures f : RobotFeatures.values()) {
            if (featureState[f.index]) {
                features[f.index].stopFeature();
                featureState[f.index] = false;
            }
        }
    }

    public Control getControl() {
        return control;
    }

    public void setControl(Control control) {
        this.control = control;
    }

    public RobotFeature getRobotFeatures(RobotFeatures f) {
        return features[f.index];
    }

    public boolean isAppUp() {
        return appState;
    }

    public boolean isControlUp() {
        return controlState;
    }

    public boolean isFeatureUp(RobotFeatures f) {
        return featureState[f.index];
    }

    public interface ResultCallBack {
        void result();
    }





}
