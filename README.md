
## BeagleBone Car and Android app remote control

Android app remote control: Send commands to the BeagleBone to remote control the car, and receive the camera feed.

Video:

[![See video](https://img.youtube.com/vi/QA3LeDqKk1c/0.jpg)](https://www.youtube.com/watch?v=QA3LeDqKk1c)
